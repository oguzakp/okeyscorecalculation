package com.okey.play;

import com.okey.bean.Hand;
import com.okey.bean.Point;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Stack;

/**
 * Created by Oguz.Akpinar on 01.04.2018.
 * OKEY / com.okey.play.
 */
public class PrepareGame {

	public static Point selectPoint() {
		int selectedValue = getRandom(51);
		Point selected = new Point();
		selected.setValue(selectedValue);
		selected.setSelected(true);
		return selected;
	}

	public static Stack<Point> prepareGame(Point selected) {
		Point[] mixedPoints = new Point[105];
		int okeyPoint = selected.getValue() % 13 == 0 ? selected.getValue() - 12 : selected.getValue() + 1;
		System.out.println("Okey Point is -> " + okeyPoint);
		for(int i = 0; i <= 52; i++){
			int selectedValue = getRandom(105);
			int count = 0;
			do{
				Point point = new Point();
				point.setValue(i);
				if(selected.getValue() == point.getValue()){
					point.setSelected(true);
				} else if(okeyPoint == point.getValue()){
					point.setOkey(true);
				} else if(i == 52){
					point.setFakeOkey(true);
					point.setValue(okeyPoint);
				}
				applyValue(point, selectedValue, mixedPoints);
				count++;
			}while(count < 2 && selected.getValue() != i);
		}
		mixOnce(mixedPoints);
		Stack<Point> returnValue = new Stack<>();
		Arrays.stream(mixedPoints).forEach(returnValue::push);
		return returnValue;
	}

	private static void mixOnce(Point[] mixedPoints){
		for(int i = 0; i < 1000; i++){
			int mixedArea = getRandom(105);
			int next;
			if(mixedArea == 0 || (mixedArea != 105 && upOrDown())){
				next = mixedArea + getRandom(105 - mixedArea);
			}else {
				next = mixedArea - getRandom(mixedArea);
			}
			Point p = mixedPoints[mixedArea];
			mixedPoints[mixedArea] = mixedPoints[next];
			mixedPoints[next] = p;
		}
	}

	private static void applyValue(Point value, int location, Point[] mixedPoints) {
		if(mixedPoints[location] == null)
			mixedPoints[location] = value;
		else{
			applyValue(value, location, mixedPoints, upOrDown());
		}
	}

	private static void applyValue(Point value, int location, Point[] mixedPoints, boolean upOrDown) {
		location = upOrDown ? location + 1 : location - 1;
		if(location < 0) location = 104;
		location = location % 105;
		if(mixedPoints[location] == null)
			mixedPoints[location] = value;
		else
			applyValue(value, location, mixedPoints, upOrDown);
	}

	private static boolean upOrDown() {
		return getRandom(2) == 0;
	}

	private static int getRandom(int range) {
		return Double.valueOf(new Random().nextDouble() * 1000).intValue() % range;
	}

	public static List<Hand> prepareHand(Stack<Point> pointList){
		List<Hand> returnList = new ArrayList<>();
		int selected = getRandom(4);
		for(int i = 0; i < 4; i++){
			List<Point> points = getHandPoints(pointList);
			if(i == selected){
				points.add(pointList.pop());
			}
			Hand hand = new Hand("Gamer "+(i+1), points);
			returnList.add(hand);
		}
		returnList.get(getRandom(4)).getPoints().add(pointList.pop());
		return returnList;
	}

	private static List<Point> getHandPoints(Stack<Point> pointList){
		List<Point> points = new ArrayList<>();
		for(int i = 0; i < 14; i++){
			points.add(pointList.pop());
		}
		return points;
	}
}
