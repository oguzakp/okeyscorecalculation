package com.okey.play;

import com.okey.bean.Hand;
import com.okey.bean.Point;
import com.okey.bean.PointTree;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oguz.Akpinar on 01.04.2018.
 * OKEY / com.okey.play.
 */
public class HandScoreCalculator {

	private List<PointTree> pointTrees;

	public List<PointTree> getPointTrees() {
		return pointTrees;
	}

	public void prepareCalculator(Hand hand){
		pointTrees = new ArrayList<>();
		hand.getPoints().stream().filter(p -> !p.isOkey()).forEach(p -> {
			pointTrees.add(new PointTree(p, null));
		});
		pointTrees.forEach(pt -> addNextOpportunity(hand, OrderType.NON, pt));
	}

	private PointTree getNonOkeyLast(PointTree lastPointTree){
		if(lastPointTree.getPoint().isOkey()){
			return getNonOkeyLast(lastPointTree.getParent());
		}
		return lastPointTree;
	}

	private PointTree getNonOkeyParent(PointTree pointTree){
		if(pointTree.getParent() != null && pointTree.getParent().getPoint().isOkey()){
			return getNonOkeyParent(pointTree.getParent());
		}
		return pointTree.getParent();
	}

	private Point getNearestSerialVal(Hand hand, boolean up, Integer value){
		Integer foundValue = addValueCount(value, 1, up);
		if(foundValue == null)
			return null;
		return hand.getPoints().stream().filter(p -> p.getValue() == foundValue && !p.isOkey()).findAny().orElse(null);
	}

	private Point getNearestParallelVal(Hand hand, PointTree lastPointTree){
		return hand.getPoints().stream().filter(p -> p.getValue() %13 == lastPointTree.getPoint().getValue() % 13 && !usedInValue(p, lastPointTree) && !p.isOkey()).findAny().orElse(null);
	}

	private boolean usedIn(Point p, PointTree lastPointTree){
		if(lastPointTree.getPoint().equals(p))
			return true;
		else if(lastPointTree.getParent() != null)
			return usedIn(p, lastPointTree.getParent());
		return false;
	}

	private boolean usedInValue(Point p, PointTree lastPointTree){
		if(lastPointTree.getPoint().getValue() == p.getValue())
			return true;
		else if(lastPointTree.getParent() != null)
			return usedInValue(p, lastPointTree.getParent());
		return false;
	}

	private Point getNotUsedOkey(Hand hand, PointTree lastPointTree){
		return hand.getPoints().stream().filter(p -> p.isOkey() && !usedIn(p, lastPointTree)).findFirst().orElse(null);
	}

	private Integer getLastValue(PointTree lastPointTree){
		if(!lastPointTree.getPoint().isOkey())
			return lastPointTree.getPoint().getValue();
		else {
			PointTree last = getNonOkeyLast(lastPointTree);
			PointTree prev = getNonOkeyParent(last);
			int count = 1;
			PointTree tree = lastPointTree;
			while(!tree.getParent().equals(last)){
				tree = tree.getParent();
				count++;
			}
			if(prev.getPoint().getValue() < last.getPoint().getValue()){
				return addValueCount(last.getPoint().getValue(), count,true);
			}else {
				return addValueCount(last.getPoint().getValue(), count,false);
			}
		}
	}

	private Integer addValueCount(Integer value, int count, boolean up){
		if(value % 13 == 0 && !up)
			return null;
		int foundValue = value + (value % 13 == 12 && up ? -12 : (up ? 1 : -1));
		count --;
		if(count == 0)
			return foundValue;
		return addValueCount(foundValue, count, up);
	}

	private int getParallelCount(PointTree lastPointTree){
		int count = 1;
		PointTree tree = lastPointTree;
		while(tree.getParent() != null){
			tree = tree.getParent();
			count ++;
		}
		return count;
	}

	private void addNextOpportunity(Hand hand, OrderType type, PointTree lastPointTree){
		PointTree last = getNonOkeyLast(lastPointTree);
		switch(type){
			case NON:
				Point next = getNearestSerialVal(hand, true, lastPointTree.getPoint().getValue());
				if(next != null){
					PointTree newOne = new PointTree(next, lastPointTree);
					lastPointTree.getNextPointOpportunities().add(newOne);
					addNextOpportunity(hand, OrderType.SERIAL, newOne);
				}else {
					Integer value = addValueCount(lastPointTree.getPoint().getValue(), 1, true);
					if(value != null){
						next = getNearestSerialVal(hand, true, lastPointTree.getPoint().getValue());
						if(next != null){
							Point okey = getNotUsedOkey(hand, lastPointTree);
							PointTree okTree = new PointTree(okey, lastPointTree);
							lastPointTree.getNextPointOpportunities().add(okTree);

							PointTree newOne = new PointTree(next, okTree);
							okTree.getNextPointOpportunities().add(newOne);
							addNextOpportunity(hand, OrderType.SERIAL, newOne);
						}
					}
				}
				next = getNearestSerialVal(hand, false, lastPointTree.getPoint().getValue());
				if(next != null){
					PointTree newOne = new PointTree(next, lastPointTree);
					lastPointTree.getNextPointOpportunities().add(newOne);
					addNextOpportunity(hand, OrderType.SERIAL, newOne);
				}else {
					Integer value = addValueCount(lastPointTree.getPoint().getValue(), 1, false);
					if(value != null){
						next = getNearestSerialVal(hand, false, lastPointTree.getPoint().getValue());
						if(next != null){
							Point okey = getNotUsedOkey(hand, lastPointTree);
							PointTree okTree = new PointTree(okey, lastPointTree);
							lastPointTree.getNextPointOpportunities().add(okTree);

							PointTree newOne = new PointTree(next, okTree);
							okTree.getNextPointOpportunities().add(newOne);
							addNextOpportunity(hand, OrderType.SERIAL, newOne);
						}
					}
				}

				next = getNearestParallelVal(hand, lastPointTree);
				if(next != null){
					PointTree newOne = new PointTree(next, lastPointTree);
					lastPointTree.getNextPointOpportunities().add(newOne);
					addNextOpportunity(hand, OrderType.PARALLEL, newOne);
				}
				break;
			case SERIAL:
				PointTree prev = getNonOkeyParent(last);
				if(prev != null){
					boolean up = prev.getPoint().getValue() < last.getPoint().getValue();
					next = getNearestSerialVal(hand, up, getLastValue(lastPointTree));
					PointTree newOne = null;
					if(next != null){
						newOne = new PointTree(next, lastPointTree);
						lastPointTree.getNextPointOpportunities().add(newOne);
					} else if(addValueCount(getLastValue(lastPointTree), 1, up) != null){
						Point okey = getNotUsedOkey(hand, lastPointTree);
						if(okey != null){
							newOne = new PointTree(okey, lastPointTree);
							lastPointTree.getNextPointOpportunities().add(newOne);
						}
					}
					if(newOne != null)
						addNextOpportunity(hand, OrderType.SERIAL, newOne);
				}
				break;
			case PARALLEL:
				next = getNearestParallelVal(hand, last);
				PointTree newOne = null;
				if(next != null){
					newOne = new PointTree(next, lastPointTree);
					lastPointTree.getNextPointOpportunities().add(newOne);
				}else if(getParallelCount(lastPointTree) < 3){
					Point okey = getNotUsedOkey(hand, lastPointTree);
					if(okey != null){
						newOne = new PointTree(okey, lastPointTree);
						lastPointTree.getNextPointOpportunities().add(newOne);
					}
				}
				if(newOne != null)
					addNextOpportunity(hand, OrderType.PARALLEL, newOne);
				break;
		}
	}


	private enum OrderType{
		PARALLEL, SERIAL, NON;
	}

	private List<PointTree> getLeaves(List<PointTree> pointTreesList){
		List<PointTree> leaves = new ArrayList<>();
		for(PointTree p : pointTreesList){
			if(p.getNextPointOpportunities().isEmpty())
				leaves.add(p);
			else
				leaves.addAll(getLeaves(p.getNextPointOpportunities()));
		}
		return leaves;
	}

	private PointTree deepestLeaf(List<PointTree> leaves){
		int length = 0;
		PointTree longest = null;
		for(PointTree p : leaves){
			PointTree tree = p;
			int count = 1;
			while(tree.getParent() != null){
				tree = tree.getParent();
				count++;
			}
			if(count > length){
				length = count;
				longest = p;
			}
		}
		return longest;
	}

	public Integer calculateScore(Hand hand){
		List<Point> selected = new ArrayList<>(hand.getPoints());
		int score = hand.getOkeyCount() * 2;
		PointTree deepest = null;
		int rowCount = 0;
		do{
			prepareCalculator(hand);
			List<PointTree> leaves = getLeaves(pointTrees);
			deepest = deepestLeaf(leaves);
			rowCount = getTreeCount(deepest);
			if(rowCount > 1){
				score += score(deepest);
				toStringAppender(hand, deepest);
				PointTree finalDeepest = deepest;
				hand.getPoints().removeIf(p -> usedIn(p, finalDeepest));
			}
		}while(rowCount > 1);

		hand.setPoints(selected);
		return score;
	}

	private void toStringAppender(Hand hand, PointTree point){
		hand.getAppender().append("[");
		PointTree tree = point;
		while(tree.getParent() != null){
			hand.getAppender().append(tree.getPoint().getValue());
			hand.getAppender().append(",");
			tree = tree.getParent();
		}
		hand.getAppender().append(tree.getPoint().getValue());
		hand.getAppender().append("]");
	}

	private int score(PointTree tree){
		int count = getTreeCount(tree);
		boolean isOkeyUsed = isOkeyUsed(tree);
		return count - (isOkeyUsed ? 1 : 0);
	}

	private int getTreeCount(PointTree tree) {
		int count = 1;
		while(tree.getParent() != null){
			count++;
			tree = tree.getParent();
		}
		return count;
	}

	private boolean isOkeyUsed(PointTree tree){
		if(tree.getPoint().isOkey())
			return true;
		else if(tree.getParent() != null)
			return isOkeyUsed(tree.getParent());
		else return false;
	}
}
