package com.okey.play;

import com.okey.bean.Hand;
import com.okey.bean.Point;

import java.util.Comparator;
import java.util.List;
import java.util.Stack;

/**
 * Created by Oguz.Akpinar on 01.04.2018.
 * OKEY / com.okey.play.
 */
public class Mediator {

	public static void main(String[] args){
		Point selected = PrepareGame.selectPoint();
		Stack<Point> gamePoints = PrepareGame.prepareGame(selected);
		List<Hand> handList = PrepareGame.prepareHand(gamePoints);
		handList.forEach(h -> System.out.println("Hand is -> " + h.toString()));
		while(handList.stream().anyMatch(h -> h.getHandScore() == null));
		handList.stream().max(Comparator.comparing(Hand::getHandScore)).ifPresent(mostScored -> System.out.println("Winner id -> " + mostScored.toString()));
	}

}
