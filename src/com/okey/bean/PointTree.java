package com.okey.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oguz.Akpinar on 01.04.2018.
 * OKEY / com.okey.bean.
 */
public class PointTree {
	private Point point;
	private PointTree parent;
	private List<PointTree> nextPointOpportunities;

	public PointTree(Point point, PointTree parent) {
		this.point = point;
		this.parent = parent;
		nextPointOpportunities = new ArrayList<>();
	}

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}

	public PointTree getParent() {
		return parent;
	}

	public void setParent(PointTree parent) {
		this.parent = parent;
	}

	public List<PointTree> getNextPointOpportunities() {
		return nextPointOpportunities;
	}

	public void setNextPointOpportunities(List<PointTree> nextPointOpportunities) {
		this.nextPointOpportunities = nextPointOpportunities;
	}
}
