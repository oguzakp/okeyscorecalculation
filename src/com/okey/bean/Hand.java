package com.okey.bean;

import com.okey.play.HandScoreCalculator;

import java.util.List;

/**
 * Created by Oguz.Akpinar on 01.04.2018.
 * OKEY / com.okey.bean.
 */
public class Hand extends Thread{

	private String gamerName;
	private List<Point> points;
	private Integer handScore;
	private int okeyCount;
	private StringBuilder appender = new StringBuilder();

	public Hand(String gamerName, List<Point> points) {
		this.gamerName = gamerName;
		this.points = points;
		okeyCount = Long.valueOf(points.stream().filter(Point::isOkey).count()).intValue();
		start();
	}

	public StringBuilder getAppender() {
		return appender;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}

	public int getOkeyCount() {
		return okeyCount;
	}

	public List<Point> getPoints() {
		return points;
	}

	public Integer getHandScore() {
		return handScore;
	}

	private void calculateScore(){
		points.stream().filter(Point::isSelected).findAny().ifPresent(selectet -> System.out.println("Selected found for "+ gamerName + ". Point is " + selectet.toString()));
		HandScoreCalculator calculator = new HandScoreCalculator();
		handScore = calculator.calculateScore(this);
	}

	@Override
	public void run() {
		calculateScore();
	}

	@Override
	public String toString() {
		return gamerName + " " + getPoints().toString() + "\n\r" +appender.toString();
	}
}
