package com.okey.bean;

/**
 * Created by Oguz.Akpinar on 01.04.2018.
 * OKEY / com.okey.bean.
 */
public class Point {

	private boolean selected;
	private boolean okey;
	private boolean fakeOkey;
	private int value;

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isOkey() {
		return okey;
	}

	public void setOkey(boolean okey) {
		this.okey = okey;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return Integer.toString(value);
	}

	public boolean isFakeOkey() {
		return fakeOkey;
	}

	public void setFakeOkey(boolean fakeOkey) {
		this.fakeOkey = fakeOkey;
	}
}
